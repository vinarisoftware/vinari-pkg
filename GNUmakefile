#	Copyright (c) 2015 - 2025, Vinari Software
#	All rights reserved.
#
#	Redistribution and use in source and binary forms, with or without
#	modification, are permitted provided that the following conditions are met:
#
#	1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#	2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#	3. Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
#	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

BIN_NAME=vinari-pkg
LINK_NAME=vpkg
I18N_DIR=./i18n

.PHONY: create-po
create-po:
	@mkdir -p $(I18N_DIR)/
	@xgettext --language=shell --add-comments --sort-output -o $(I18N_DIR)/$(BIN_NAME).pot ./$(BIN_NAME)
	@msginit --input=$(I18N_DIR)/$(BIN_NAME).pot --no-translator --locale=es --output=$(I18N_DIR)/es.po
	@msginit --input=$(I18N_DIR)/$(BIN_NAME).pot --no-translator --locale=it --output=$(I18N_DIR)/it.po
	@msginit --input=$(I18N_DIR)/$(BIN_NAME).pot --no-translator --locale=fr --output=$(I18N_DIR)/fr.po
	@msginit --input=$(I18N_DIR)/$(BIN_NAME).pot --no-translator --locale=pt --output=$(I18N_DIR)/pt.po

.PHONY: update-po
update-po:
	@xgettext --language=shell --add-comments --sort-output -o $(I18N_DIR)/$(BIN_NAME).pot ./$(BIN_NAME)
	@msgmerge --update $(I18N_DIR)/es.po $(I18N_DIR)/$(BIN_NAME).pot
	@msgmerge --update $(I18N_DIR)/it.po $(I18N_DIR)/$(BIN_NAME).pot
	@msgmerge --update $(I18N_DIR)/fr.po $(I18N_DIR)/$(BIN_NAME).pot
	@msgmerge --update $(I18N_DIR)/pt.po $(I18N_DIR)/$(BIN_NAME).pot

.PHONY: make-mo
make-mo:
	@mkdir -p $(I18N_DIR)/locale/es/LC_MESSAGES/
	@mkdir -p $(I18N_DIR)/locale/fr/LC_MESSAGES/
	@mkdir -p $(I18N_DIR)/locale/it/LC_MESSAGES/
	@mkdir -p $(I18N_DIR)/locale/pt/LC_MESSAGES/

	@msgfmt -o $(I18N_DIR)/locale/es/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/es.po
	@msgfmt -o $(I18N_DIR)/locale/fr/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/fr.po
	@msgfmt -o $(I18N_DIR)/locale/it/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/it.po
	@msgfmt -o $(I18N_DIR)/locale/pt/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/pt.po

.PHONY: clean-mo
clean-mo:
	@rm -rf $(I18N_DIR)/locale/

.PHONY: install
install:
	@gzip -9kn ./man/$(BIN_NAME).8
	@mv ./man/$(BIN_NAME).8.gz /usr/share/man/man8/
	@cp -r ./bash-completions/$(BIN_NAME).completion /usr/share/bash-completion/completions/$(BIN_NAME)
	@cp -r ./bash-completions/$(LINK_NAME).completion /usr/share/bash-completion/completions/$(LINK_NAME)
	@cp -r ./$(BIN_NAME) /usr/sbin/

	@ln -fs /usr/sbin/$(BIN_NAME) /usr/sbin/$(LINK_NAME)

	@mkdir -p $(I18N_DIR)/locale/es/LC_MESSAGES/
	@mkdir -p $(I18N_DIR)/locale/fr/LC_MESSAGES/
	@mkdir -p $(I18N_DIR)/locale/it/LC_MESSAGES/
	@mkdir -p $(I18N_DIR)/locale/pt/LC_MESSAGES/

	@msgfmt -o $(I18N_DIR)/locale/es/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/es.po
	@msgfmt -o $(I18N_DIR)/locale/fr/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/fr.po
	@msgfmt -o $(I18N_DIR)/locale/it/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/it.po
	@msgfmt -o $(I18N_DIR)/locale/pt/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)/pt.po

	@cp $(I18N_DIR)/locale/es/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/es/LC_MESSAGES/
	@cp $(I18N_DIR)/locale/fr/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/fr/LC_MESSAGES/
	@cp $(I18N_DIR)/locale/it/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/it/LC_MESSAGES/
	@cp $(I18N_DIR)/locale/pt/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/pt/LC_MESSAGES/

	@rm -rf $(I18N_DIR)/locale/

.PHONY: uninstall
uninstall:
	@rm -rf /usr/share/man/man8/$(BIN_NAME).8.gz
	@rm -rf /usr/share/bash-completion/completions/$(BIN_NAME)
	@rm -rf /usr/sbin/$(BIN_NAME)

	@rm -rf /usr/sbin/$(LINK_NAME)
	@rm -rf /usr/share/bash-completion/completions/$(LINK_NAME)

	@rm -rf /usr/share/locale/es/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/fr/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/it/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/pt/LC_MESSAGES/$(BIN_NAME).mo
