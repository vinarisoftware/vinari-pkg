* vinari-pkg

[[https://gitlab.com/vinarisoftware/vinari-pkg/-/tree/master][MASTER-Branch]]

*** VINARI-PKG is a shell script that wraps the complex usage of the Apt and Flatpak package manager under one set of commands:

	+ Easy to use and script with.
	+ Friendly with new users.

*** Dependencies to compile vinari-os-cli-coreutils:
	+ gettext
	+ apt
	+ flatpak
	+ make

*** Dependencies to run 'vinari-os-cli-coreutils': 
	+ bash
	+ apt
	+ flatpak
	+ coreutils
	

* Contact with the developers

| Contact                | Link                          |
|------------------------+-------------------------------|
| Vinari Software (Mail) | vinarisoftware@protonmail.com |
| Vinari Software (Chat) | [[https://vinarisoftware.wixsite.com/vinari/contacta-con-nosotros][Vinari Software website]]       |
| Vinari OS (Mail)       | [[https://vinarios.me][Vinari OS website]]             |



